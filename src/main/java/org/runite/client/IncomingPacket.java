package org.runite.client;

import org.rs09.Discord;
import org.rs09.SlayerTracker;
import org.rs09.XPGainDraw;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class IncomingPacket {
    /**
     * Named And Identified
     */
    //map
    static final int UPDATE_CURRENT_LOCATION = 26;
    static final int CLEAR_REGION_CHUNK = 112;
    static final int UPDATE_SCENE_GRAPH = 162;
    static final int DYNAMIC_SCENE_GRAPH = 214;
    static final int CHUNK_UPDATE_BUFFER = 230;

    //render
    static final int RENDER_NPC_PACKET = 32;
    static final int PLAYER_RENDER_PACKET = 225;

    //var{p,c,bit}
    static final int SET_VARBIT_SMALL = 37;
    static final int SET_VARBIT_LARGE = 84;
    static final int SET_VARP_SMALL = 60;
    static final int SET_VARP_LARGE = 226;
    static final int SET_VARC_SMALL = 65;
    static final int SET_VARC_LARGE = 69;

    //chat
    static final int RECEIVE_GAME_MESSAGE = 70;
    static final int RECEIVE_PM = 2;
    static final int CHAT_SETTING_UPDATE = 232;

    //{friends,ignore,clan} packets
    static final int CLAN_CHAT_UPDATE = 55;
    static final int UPDATE_FRIEND_INFO = 62;
    static final int UPDATE_FRIENDLIST_STATE = 197;
    static final int UPDATE_IGNORE_LIST = 126;

    //interfaces
    static final int SET_COMPONENT_VISIBILITY = 21;
    static final int ANIMATE_COMPONENT = 36;
    static final int SEND_ITEM_TO_COMPONENT = 50;
    static final int SEND_PLAYER_TO_COMPONENT = 66;
    static final int SEND_NPC_TO_COMPONENT = 73;
    static final int SEND_MODEL_TO_COMPONENT = 130;
    static final int REPOSITION_COMPONENT = 119;
    static final int OPEN_WINDOWPANE = 145;
    static final int RESET_IFACE = 149;
    static final int OPEN_INTERFACE = 155;
    static final int SET_INTERFACE_SETTINGS = 165;
    static final int SET_COMPONENT_TEXT = 171;

    //misc
    static final int UPDATE_SKILL_XP = 38;
    static final int RUN_CS2 = 115;
    static final int SYSTEM_UPDATE = 85;
    static final int PLAY_SONG = 4;
    static final int PLAY_JINGLE = 208;
    static final int SET_INTERACTION = 44;
    static final int LOGOUT_PACKET = 86;
    static final int GRAND_EXCHANGE_UPDATE = 116;
    static final int CLEAR_MINIMAP_FLAG = 153;
    static final int SET_MINIMAP_STATE = 192;
    static final int CLEAR_CONTAINER = 144;
    static final int UPDATE_CONTAINER_SLOTBASED = 22;
    static final int UPDATE_CONTAINER_NONSLOT = 105;
    static final int SET_HINT_ICON = 217;
    static final int RUN_ENERGY_UPDATE = 234;

    /**
     * Unidentified/Uncertain (probable mislabel/misuse by Arios)
     */
    static final int INCOMING_PACKET_0 = 0;
    static final int INCOMING_PACKET_9 = 9;
    static final int INCOMING_PACKET_13 = 13;
    static final int INCOMING_PACKET_14 = 14;
    static final int INCOMING_PACKET_16 = 16;
    static final int INCOMING_PACKET_17 = 17;
    static final int INCOMING_PACKET_20 = 20;
    static final int INCOMING_PACKET_24 = 24;
    static final int INCOMING_PACKET_27 = 27;
    static final int INCOMING_PACKET_33 = 33;
    static final int INCOMING_PACKET_42 = 42;
    static final int INCOMING_PACKET_48 = 48;
    static final int INCOMING_PACKET_54 = 54;
    static final int INCOMING_PACKET_56 = 56;
    static final int INCOMING_PACKET_71 = 71;
    static final int INCOMING_PACKET_81 = 81;
    static final int INCOMING_PACKET_89 = 89;
    static final int INCOMING_PACKET_97 = 97;
    static final int INCOMING_PACKET_102 = 102;
    static final int INCOMING_PACKET_104 = 104;
    static final int INCOMING_PACKET_111 = 111;
    static final int INCOMING_PACKET_114 = 114;
    static final int INCOMING_PACKET_121 = 121;
    static final int INCOMING_PACKET_123 = 123;
    static final int INCOMING_PACKET_125 = 125;
    static final int INCOMING_PACKET_128 = 128;
    static final int INCOMING_PACKET_131 = 131;
    static final int INCOMING_PACKET_132 = 132;
    static final int INCOMING_PACKET_135 = 135;
    static final int INCOMING_PACKET_141 = 141;
    static final int INCOMING_PACKET_142 = 142;
    static final int INCOMING_PACKET_154 = 154;
    static final int INCOMING_PACKET_159 = 159;
    static final int INCOMING_PACKET_160 = 160;
    static final int INCOMING_PACKET_164 = 164;
    static final int INCOMING_PACKET_169 = 169;
    static final int INCOMING_PACKET_172 = 172;
    static final int INCOMING_PACKET_176 = 176;
    static final int INCOMING_PACKET_179 = 179;
    static final int INCOMING_PACKET_187 = 187;
    static final int INCOMING_PACKET_191 = 191;
    static final int INCOMING_PACKET_195 = 195;
    static final int INCOMING_PACKET_196 = 196;
    static final int INCOMING_PACKET_202 = 202;
    static final int INCOMING_PACKET_207 = 207;
    static final int INCOMING_PACKET_209 = 209;
    static final int INCOMING_PACKET_220 = 220;
    static final int INCOMING_PACKET_240 = 240;
    static final int INCOMING_PACKET_247 = 247;
    static final int INCOMING_PACKET_235 = 235;


    public static boolean parse()
    {
        int opcode = Unsorted.incomingOpcode;
        int inTutorialIsland = PacketParser.inTutorialIsland;

        if(opcode == SET_VARP_SMALL)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            byte var69 = BufferedDataStream.incomingBuffer.readSignedNegativeByte();
            VarpHelpers.setVarp(var69, nodeModelId);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == RUN_CS2)
        {
            RSString argTypes;
            int counter;
            int packetCount = BufferedDataStream.incomingBuffer.readUnsignedShort();
            argTypes = BufferedDataStream.incomingBuffer.readString();
            Object[] scriptArgs = new Object[argTypes.length() - -1];
            for (counter = argTypes.length() + -1; counter >= 0; --counter) {
                if ('s' == argTypes.charAt(counter)) {
                    scriptArgs[1 + counter] = BufferedDataStream.incomingBuffer.readString();
                } else {
                    scriptArgs[1 + counter] = BufferedDataStream.incomingBuffer.readInt();
                }
            }

            scriptArgs[0] = BufferedDataStream.incomingBuffer.readInt(); // script ID
            Class146.updateInterfacePacketCounter(packetCount);
            CS2Script script = new CS2Script();
            script.arguments = scriptArgs;
            Class43.runCS2(script);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == RECEIVE_GAME_MESSAGE)
        {
            RSString message = BufferedDataStream.incomingBuffer.readString();
            RSString playerName;
            RSString var41;
            long nameAsLong;
            boolean isIgnored;
            int var30;
            if (message.endsWith(RSString.parse(":tradereq:"))) {
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), 65), 0);
                nameAsLong = playerName.toLong();
                isIgnored = false;

                for (var30 = 0; Class3_Sub28_Sub5.anInt3591 > var30; ++var30) {
                    if (nameAsLong == Class114.ignores[var30]) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && inTutorialIsland == 0) {
                    BufferedDataStream.addChatMessage(playerName, 4, TextCore.HasWishToTrade, (byte) -83 + 82);
                }
            } else if (message.endsWith(TextCore.cmdChalReq)) {
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), 75), 0);
                nameAsLong = playerName.toLong();
                isIgnored = false;

                for (var30 = 0; var30 < Class3_Sub28_Sub5.anInt3591; ++var30) {
                    if (Class114.ignores[var30] == nameAsLong) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && inTutorialIsland == 0) {
                    var41 = message.substring(1 + message.indexOf(RSString.parse(":"), 101), message.length() + -9, (byte) -83 ^ -83);
                    BufferedDataStream.addChatMessage(playerName, 8, var41, (byte) -83 ^ 82);
                }
            } else if (message.endsWith(TextCore.HasAssistRequest)) {
                isIgnored = false;
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), 96), 0);
                nameAsLong = playerName.toLong();

                for (var30 = 0; var30 < Class3_Sub28_Sub5.anInt3591; ++var30) {
                    if (nameAsLong == Class114.ignores[var30]) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && inTutorialIsland == 0) {
                    BufferedDataStream.addChatMessage(playerName, 10, RSString.parse(""), -1);
                }
            } else if (message.endsWith(TextCore.HasClan)) {
                playerName = message.substring(0, message.indexOf(TextCore.HasClan, (byte) -83 ^ -50), 0);
                BufferedDataStream.addChatMessage(RSString.parse(""), 11, playerName, -1);
            } else if (message.endsWith(TextCore.HasTrade)) {
                playerName = message.substring(0, message.indexOf(TextCore.HasTrade, 102), 0);
                if (0 == inTutorialIsland) {
                    BufferedDataStream.addChatMessage(RSString.parse(""), 12, playerName, -1);
                }
            } else if (message.endsWith(TextCore.HasAssist)) {
                playerName = message.substring(0, message.indexOf(TextCore.HasAssist, 121), 0);
                if (inTutorialIsland == 0) {
                    BufferedDataStream.addChatMessage(RSString.parse(""), 13, playerName, -1);
                }
            } else if (message.endsWith(TextCore.HasDuelStake)) {
                isIgnored = false;
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), 115), 0);
                nameAsLong = playerName.toLong();

                for (var30 = 0; Class3_Sub28_Sub5.anInt3591 > var30; ++var30) {
                    if (nameAsLong == Class114.ignores[var30]) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && inTutorialIsland == 0) {
                    BufferedDataStream.addChatMessage(playerName, 14, RSString.parse(""), -1);
                }
            } else if (message.endsWith(TextCore.HasDuelFriend)) {
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), 118), 0);
                isIgnored = false;
                nameAsLong = playerName.toLong();

                for (var30 = 0; var30 < Class3_Sub28_Sub5.anInt3591; ++var30) {
                    if (nameAsLong == Class114.ignores[var30]) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && 0 == inTutorialIsland) {
                    BufferedDataStream.addChatMessage(playerName, 15, RSString.parse(""), -1);
                }
            } else if (message.endsWith(TextCore.HasClanRequest)) {
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), (byte) -83 + 138), 0);
                nameAsLong = playerName.toLong();
                isIgnored = false;

                for (var30 = 0; var30 < Class3_Sub28_Sub5.anInt3591; ++var30) {
                    if (Class114.ignores[var30] == nameAsLong) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && inTutorialIsland == 0) {
                    BufferedDataStream.addChatMessage(playerName, 16, RSString.parse(""), -1);
                }
            } else if (message.endsWith(TextCore.HasAllyReq)) {
                playerName = message.substring(0, message.indexOf(RSString.parse(":"), (byte) -83 + 189), (byte) -83 + 83);
                isIgnored = false;
                nameAsLong = playerName.toLong();

                for (var30 = 0; var30 < Class3_Sub28_Sub5.anInt3591; ++var30) {
                    if (nameAsLong == Class114.ignores[var30]) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored && inTutorialIsland == 0) {
                    var41 = message.substring(1 + message.indexOf(RSString.parse(":"), 92), message.length() - 9, (byte) -83 ^ -83);
                    BufferedDataStream.addChatMessage(playerName, 21, var41, -1);
                }
            } else {
                BufferedDataStream.addChatMessage(RSString.parse(""), 0, message, (byte) -83 + 82);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_123)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            RSString var58 = BufferedDataStream.incomingBuffer.readString();
            Class146.updateInterfacePacketCounter(var19);
            InterfaceWidget.setWidgetText(var58, nodeModelId);


            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == CHUNK_UPDATE_BUFFER)
        {
            Class39.currentChunkY = BufferedDataStream.incomingBuffer.readUnsignedByte128();
            Class39.currentChunkX = BufferedDataStream.incomingBuffer.readUnsigned128Byte();

            while (BufferedDataStream.incomingBuffer.index < Unsorted.incomingPacketLength) {
                Unsorted.incomingOpcode = BufferedDataStream.incomingBuffer.readUnsignedByte();
                Class39.parseChunkPacket((byte) -82);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == CLEAR_MINIMAP_FLAG)
        {
            Unsorted.incomingOpcode = -1;
            Class65.anInt987 = 0;
            return true;
        }

        if(opcode == INCOMING_PACKET_220)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV2();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            Class146.updateInterfacePacketCounter(modelId);
            TextureOperation29.method327(var19, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_81)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            BufferedDataStream.incomingBuffer.readSignedByte();
            long nameAsLong = BufferedDataStream.incomingBuffer.readLong();
            int var29 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var36 = BufferedDataStream.incomingBuffer.readMedium();
            int clanChatIcon = BufferedDataStream.incomingBuffer.readUnsignedByte();
            boolean var63 = false;
            int var11 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            long var55 = (var29 << 32) + var36;
            int var54 = 0;

            label1521:
            while (true) {
                if (100 > var54) {
                    if (Class163_Sub2_Sub1.aLongArray4017[var54] != var55) {
                        ++var54;
                        continue;
                    }

                    var63 = true;
                    break;
                }

                if (1 >= clanChatIcon) {
                    for (var54 = 0; var54 < Class3_Sub28_Sub5.anInt3591; ++var54) {
                        if (var2 == Class114.ignores[var54]) {
                            var63 = true;
                            break label1521;
                        }
                    }
                }
                break;
            }

            if (!var63 && 0 == inTutorialIsland) {
                Class163_Sub2_Sub1.aLongArray4017[MouseListeningClass.anInt1921] = var55;
                MouseListeningClass.anInt1921 = (1 + MouseListeningClass.anInt1921) % 100;
                RSString var61 = QuickChat.method733(var11).method555(BufferedDataStream.incomingBuffer);
                if (clanChatIcon == 2 || 3 == clanChatIcon) {
                    MessageManager.sendGameMessage(var11, 20, var61, Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), RSString.stringCombiner(new RSString[]{RSString.parse("<img=1>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}));
                } else if (clanChatIcon == 1) {
                    MessageManager.sendGameMessage(var11, 20, var61, Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), RSString.stringCombiner(new RSString[]{RSString.parse("<img=0>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}));
                } else {
                    MessageManager.sendGameMessage(var11, 20, var61, Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString());
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == CLAN_CHAT_UPDATE)
        {
            Class167.anInt2087 = PacketParser.anInt3213;
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            int var6;
            int chatIcon;
            int clanChatIcon;
            int var11;
            boolean var32;

            if (var2 == 0) {
                Class161.aString_2035 = null;
                Unsorted.incomingOpcode = -1;
                RSInterface.aString_251 = null;
                PacketParser.aClass3_Sub19Array3694 = null;
                Unsorted.clanSize = 0;
                return true;
            } else {
                long nameAsLong = BufferedDataStream.incomingBuffer.readLong();
                RSInterface.aString_251 = Unsorted.method1052(nameAsLong);
                Class161.aString_2035 = Unsorted.method1052(var2);
                Player.aByte3953 = BufferedDataStream.incomingBuffer.readSignedByte();
                var6 = BufferedDataStream.incomingBuffer.readUnsignedByte();
                if (255 != var6) {
                    Unsorted.clanSize = var6;
                    Class3_Sub19[] var7 = new Class3_Sub19[100];

                    for (chatIcon = 0; chatIcon < Unsorted.clanSize; ++chatIcon) {
                        var7[chatIcon] = new Class3_Sub19();
                        var7[chatIcon].linkableKey = BufferedDataStream.incomingBuffer.readLong();
                        var7[chatIcon].aString_2476 = Unsorted.method1052(var7[chatIcon].linkableKey);
                        var7[chatIcon].anInt2478 = BufferedDataStream.incomingBuffer.readUnsignedShort();
                        var7[chatIcon].aByte2472 = BufferedDataStream.incomingBuffer.readSignedByte();
                        var7[chatIcon].aString_2473 = BufferedDataStream.incomingBuffer.readString();
                        if (var7[chatIcon].linkableKey == PacketParser.aLong3202) {
                            Class91.aByte1308 = var7[chatIcon].aByte2472;
                        }
                    }

                    clanChatIcon = Unsorted.clanSize;

                    while (clanChatIcon > 0) {
                        var32 = true;
                        --clanChatIcon;

                        for (var11 = 0; var11 < clanChatIcon; ++var11) {
                            if (var7[var11].aString_2476.method1559(var7[var11 - -1].aString_2476) > 0) {
                                var32 = false;
                                Class3_Sub19 var9 = var7[var11];
                                var7[var11] = var7[1 + var11];
                                var7[var11 + 1] = var9;
                            }
                        }

                        if (var32) {
                            break;
                        }
                    }

                    PacketParser.aClass3_Sub19Array3694 = var7;
                }
                Unsorted.incomingOpcode = -1;
                return true;
            }
        }

        if(opcode == INCOMING_PACKET_164)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV1();
            Class136.aClass64_1778 = Class38.gameSignlink.method1449((byte) -83 ^ -82, nodeModelId);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == PLAYER_RENDER_PACKET)
        {
            PlayerRendering.renderPlayers();
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_48)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            RSString playerName = BufferedDataStream.incomingBuffer.readString();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            Class146.updateInterfacePacketCounter(nodeModelId);
            InterfaceWidget.setWidgetText(playerName, modelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == CHAT_SETTING_UPDATE)
        {
            CS2Script.anInt3101 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class24.anInt467 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class45.anInt734 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_INTERACTION)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            if (nodeModelId == 65535) {
                nodeModelId = -1;
            }

            int var19 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            RSString var56 = BufferedDataStream.incomingBuffer.readString();
            if (1 <= modelId && modelId <= 8) {
                if (var56.equalsStringIgnoreCase(TextCore.HasNull)) {
                    var56 = null;
                }

                Class91.aStringArray1299[-1 + modelId] = var56;
                TextureOperation35.anIntArray3328[modelId + -1] = nodeModelId;
                Class1.aBooleanArray54[modelId + -1] = var19 == 0;
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_VARP_LARGE)
        {
            int value = BufferedDataStream.incomingBuffer.readInt();
            int index = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            VarpHelpers.setVarp(value, index);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_COMPONENT_VISIBILITY)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedNegativeByte();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int modelId = BufferedDataStream.incomingBuffer.readIntLE();
            Class146.updateInterfacePacketCounter(var19);
            TextureOperation4.method260(modelId, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == OPEN_WINDOWPANE)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedByte128();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            Class146.updateInterfacePacketCounter(modelId);
            if (var19 == 2) {
                PacketParser.method834();
            }
            ConfigInventoryDefinition.anInt3655 = nodeModelId;
            TextureOperation20.method232(nodeModelId);
            Class124.method1746(false, (byte) -64);
            TextureOperation24.method226(ConfigInventoryDefinition.anInt3655);

            for (int counter = 0; counter < 100; ++counter) {
                Unsorted.aBooleanArray3674[counter] = true;
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_VARC_LARGE)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int var19 = BufferedDataStream.incomingBuffer.readInt();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            Class146.updateInterfacePacketCounter(nodeModelId);
            TextureOperation19.method255(modelId, var19, 1);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_141)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            RSString var56 = QuickChat.method733(modelId).method555(BufferedDataStream.incomingBuffer);
            MessageManager.sendGameMessage(modelId, 19, var56, null, Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString());
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_169)
        {
            Class162.method2204(BufferedDataStream.incomingBuffer);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_89)
        {
            TextureOperation6.method176(-117);
            BufferedDataStream.flagActiveInterfacesForUpdate();
            Class36.anInt641 += 32;
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_125) //Camera rotation?
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var6 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int var30 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class146.updateInterfacePacketCounter(nodeModelId);
            Class164_Sub1.method2238(counter, modelId, var6, var19, var30);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == ANIMATE_COMPONENT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV2();
            int var19 = BufferedDataStream.incomingBuffer.readSignedShortLE();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            Class146.updateInterfacePacketCounter(modelId);
            Class131.method1790(nodeModelId, var19);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_9)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int var19 = BufferedDataStream.incomingBuffer.readIntLE();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            Class3_Sub1 var38;
            if (counter == 65535) {
                counter = -1;
            }

            int var6 = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            if (var6 == 65535) {
                var6 = -1;
            }

            Class146.updateInterfacePacketCounter(modelId);
            for (int var30 = var6; counter >= var30; ++var30) {
                long var36 = (long) var30 + ((long) var19 << 32);
                Class3_Sub1 var47 = (Class3_Sub1) Class124.aHashTable_1659.get(var36);
                if (null != var47) {
                    var38 = new Class3_Sub1(var47.anInt2205, nodeModelId);
                    var47.unlink();
                } else if (var30 == -1) {
                    var38 = new Class3_Sub1(Objects.requireNonNull(Unsorted.getRSInterface(var19)).aClass3_Sub1_257.anInt2205, nodeModelId);
                } else {
                    var38 = new Class3_Sub1(0, nodeModelId);
                }

                Class124.aHashTable_1659.put(var36, var38);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_56)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int modelId = BufferedDataStream.incomingBuffer.readIntV1();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            if (modelId >> 30 == 0) {
                SequenceDefinition var53;
                if (modelId >> 29 != 0) {
                    int var6 = 65535 & modelId;
                    NPC var62 = NPC.npcs[var6];
                    if (null != var62) {
                        if (counter == 65535) {
                            counter = -1;
                        }
                        boolean var32 = counter == -1 || -1 == var62.anInt2842 || SequenceDefinition.getAnimationDefinition(GraphicDefinition.getGraphicDefinition(var62.anInt2842).anInt542).forcedPriority <= SequenceDefinition.getAnimationDefinition(GraphicDefinition.getGraphicDefinition(counter).anInt542).forcedPriority;

                        if (var32) {
                            var62.anInt2761 = 0;
                            var62.anInt2842 = counter;
                            var62.anInt2759 = Class44.anInt719 - -nodeModelId;
                            var62.anInt2805 = 0;
                            if (var62.anInt2759 > Class44.anInt719) {
                                var62.anInt2805 = -1;
                            }

                            var62.anInt2799 = var19;
                            var62.anInt2826 = 1;
                            if (var62.anInt2842 != -1 && Class44.anInt719 == var62.anInt2759) {
                                int var33 = GraphicDefinition.getGraphicDefinition(var62.anInt2842).anInt542;
                                if (var33 != -1) {
                                    var53 = SequenceDefinition.getAnimationDefinition(var33);
                                    if (null != var53.frames) {
                                        Unsorted.method1470(var62.yAxis, var53, var62.xAxis, false, 0);
                                    }
                                }
                            }
                        }
                    }
                } else if (modelId >> 28 != 0) {
                    int var6 = modelId & 65535;
                    Player var60;
                    if (var6 == Class3_Sub1.localIndex) {
                        var60 = Class102.player;
                    } else {
                        var60 = Unsorted.players[var6];
                    }

                    if (null != var60) {
                        if (counter == 65535) {
                            counter = -1;
                        }
                        boolean var32 = counter == -1 || var60.anInt2842 == -1 || SequenceDefinition.getAnimationDefinition(GraphicDefinition.getGraphicDefinition(var60.anInt2842).anInt542).forcedPriority <= SequenceDefinition.getAnimationDefinition(GraphicDefinition.getGraphicDefinition(counter).anInt542).forcedPriority;

                        if (var32) {
                            var60.anInt2759 = nodeModelId + Class44.anInt719;
                            var60.anInt2799 = var19;
                            var60.anInt2842 = counter;

                            var60.anInt2826 = 1;
                            var60.anInt2761 = 0;
                            var60.anInt2805 = 0;
                            if (var60.anInt2759 > Class44.anInt719) {
                                var60.anInt2805 = -1;
                            }

                            if (var60.anInt2842 != -1 && Class44.anInt719 == var60.anInt2759) {
                                int var33 = GraphicDefinition.getGraphicDefinition(var60.anInt2842).anInt542;
                                if (var33 != -1) {
                                    var53 = SequenceDefinition.getAnimationDefinition(var33);
                                    if (null != var53.frames) {
                                        Unsorted.method1470(var60.yAxis, var53, var60.xAxis, var60 == Class102.player, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                int var6 = 3 & modelId >> 28;
                int var30 = ((modelId & 268434277) >> 14) + -Class131.x1716;
                int chatIcon = (modelId & 16383) + -Texture.y1152;
                if (var30 >= 0 && chatIcon >= 0 && 104 > var30 && chatIcon < 104) {
                    chatIcon = chatIcon * 128 - -64;
                    var30 = 128 * var30 + 64;
                    PositionedGraphicObject var50 = new PositionedGraphicObject(counter, var6, var30, chatIcon, -var19 + Scenery.sceneryPositionHash(var6, 1, var30, chatIcon), nodeModelId, Class44.anInt719);
                    TextureOperation17.aLinkedList_3177.pushBack(new Class3_Sub28_Sub2(var50));
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_207)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV2();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            Class146.updateInterfacePacketCounter(var19);
            Class114.method1708(counter + (modelId << 16), nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_SKILL_XP)
        {
            BufferedDataStream.flagActiveInterfacesForUpdate();
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedByte128();//Level
            int var19 = BufferedDataStream.incomingBuffer.readIntV1();//Skillxp
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedByte();//Skill ID
            int gain = var19 - XPGainDraw.getLastXp()[modelId];
            XPGainDraw.addGain(gain,modelId);
            XPGainDraw.getLastXp()[modelId] = var19;
            Class133.anIntArray1743[modelId] = var19;//XP for Skill ID
            TextureOperation17.anIntArray3185[modelId] = nodeModelId;//Level for Skill ID
            Class3_Sub20.anIntArray2480[modelId] = 1;

            for (int counter = 0; 98 > counter; ++counter) {
                if (ItemDefinition.anIntArray781[counter] <= var19) { //Checks xp less than or equal to level 98 or 11805606 xp
                    Class3_Sub20.anIntArray2480[modelId] = counter + 2;
                }
            }
            //Calculate xp till next level
            //System.out.println("xp Gained: " + (ItemDefinition.anIntArray781[nodeModelId - 1] - var19));


            Client.anIntArray3780[Unsorted.bitwiseAnd(31, Class49.anInt815++)] = modelId;
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_104
                || opcode == INCOMING_PACKET_121
                || opcode == INCOMING_PACKET_97
                || opcode == INCOMING_PACKET_14
                || opcode == INCOMING_PACKET_202
                || opcode == INCOMING_PACKET_135
                || opcode == INCOMING_PACKET_17
                || opcode == INCOMING_PACKET_16
                || opcode == INCOMING_PACKET_240
                || opcode == INCOMING_PACKET_33
                || opcode == INCOMING_PACKET_20
                || opcode == INCOMING_PACKET_195
                || opcode == INCOMING_PACKET_179)
        {
            Class39.parseChunkPacket((byte) -99);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == RESET_IFACE)
        {
            int packetCount = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int iface = BufferedDataStream.incomingBuffer.readInt();
            Class146.updateInterfacePacketCounter(packetCount);
            Class3_Sub31 var67 = TextureOperation23.openInterfaces.get(iface);
            if (null != var67) {
                TextureOperation19.method254(true, var67);
            }

            if (null != TextureOperation27.aClass11_3087) {
                Class20.flagUpdate(TextureOperation27.aClass11_3087);
                TextureOperation27.aClass11_3087 = null;
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_187)
        {
            //set camera
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            Class146.updateInterfacePacketCounter(var19);
            GraphicDefinition.CAMERA_DIRECTION = nodeModelId;
            Unsorted.anInt2309 = modelId;
            if (Class133.anInt1753 == 2) {
                Class139.anInt1823 = Unsorted.anInt2309;
                TextureOperation28.anInt3315 = GraphicDefinition.CAMERA_DIRECTION;
            }

            Unsorted.clampCameraAngle();

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_132)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int var6 = BufferedDataStream.incomingBuffer.readInt();
            Class146.updateInterfacePacketCounter(var19);
            Unsorted.method2143((byte) -124, modelId, var6, counter, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == CLEAR_REGION_CHUNK)
        {
            Class39.currentChunkX = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class39.currentChunkY = BufferedDataStream.incomingBuffer.readUnsignedNegativeByte();

            for (int nodeModelId = Class39.currentChunkX; nodeModelId < 8 + Class39.currentChunkX; ++nodeModelId) {
                for (int var19 = Class39.currentChunkY; 8 + Class39.currentChunkY > var19; ++var19) {
                    if (null != Class39.groundItems[WorldListCountry.localPlane][nodeModelId][var19]) {
                        Class39.groundItems[WorldListCountry.localPlane][nodeModelId][var19] = null;
                        Class128.method1760(var19, nodeModelId);
                    }
                }
            }

            for (Scenery var68 = (Scenery) Scenery.sceneryList.startIteration(); null != var68; var68 = (Scenery) Scenery.sceneryList.nextIteration()) {
                if (Class39.currentChunkX <= var68.x && 8 + Class39.currentChunkX > var68.x && var68.y >= Class39.currentChunkY && 8 + Class39.currentChunkY > var68.y && var68.plane == WorldListCountry.localPlane) {
                    var68.anInt2259 = 0;
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == CLEAR_CONTAINER)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV2();
            RSInterface var65 = Unsorted.getRSInterface(nodeModelId);

            for (int modelId = 0; Objects.requireNonNull(var65).itemIds.length > modelId; ++modelId) {
                var65.itemIds[modelId] = -1;
                var65.itemIds[modelId] = 0;
            }

            Class20.flagUpdate(var65);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SEND_MODEL_TO_COMPONENT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntLE();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            if (modelId == 65535) {
                modelId = -1;
            }

            Class146.updateInterfacePacketCounter(var19);
            PacketParser.method256(-1, 1, nodeModelId, modelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_MINIMAP_STATE)
        {
            Class161.minimapState = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_13)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsigned128Byte();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedByte128();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            WorldListCountry.localPlane = var19 >> 1;
            Class102.player.updatePlayerPosition(nodeModelId, (var19 & 1) == 1, modelId);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_FRIEND_INFO)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedByte();
            boolean isIgnored = true;
            if (var2 < 0L) {
                var2 &= Long.MAX_VALUE;
                isIgnored = false;
            }

            RSString var41 = RSString.parse("");
            if (modelId > 0) {
                var41 = BufferedDataStream.incomingBuffer.readString();
            }

            RSString var46 = Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString();

            for (int var33 = 0; var33 < Class8.anInt104; ++var33) {
                if (var2 == Class50.aLongArray826[var33]) {
                    if (Unsorted.anIntArray882[var33] != modelId) {
                        Unsorted.anIntArray882[var33] = modelId;
                        if (0 < modelId) {
                            BufferedDataStream.addChatMessage(RSString.parse(""), 5, RSString.stringCombiner(new RSString[]{var46, TextCore.HasLoggedIn}), -1);
                        }

                        if (modelId == 0) {
                            BufferedDataStream.addChatMessage(RSString.parse(""), 5, RSString.stringCombiner(new RSString[]{var46, TextCore.HasLoggedOut}), -1);
                        }
                    }

                    Unsorted.aStringArray2566[var33] = var41;
                    Class57.anIntArray904[var33] = counter;
                    var46 = null;
                    Unsorted.aBooleanArray73[var33] = isIgnored;
                    break;
                }
            }

            boolean var45;
            if (null != var46 && 200 > Class8.anInt104) {
                Class50.aLongArray826[Class8.anInt104] = var2;
                Class70.aStringArray1046[Class8.anInt104] = var46;
                Unsorted.anIntArray882[Class8.anInt104] = modelId;
                Unsorted.aStringArray2566[Class8.anInt104] = var41;
                Class57.anIntArray904[Class8.anInt104] = counter;
                Unsorted.aBooleanArray73[Class8.anInt104] = isIgnored;
                ++Class8.anInt104;
            }

            Class110.anInt1472 = PacketParser.anInt3213;
            int clanChatIcon = Class8.anInt104;

            while (clanChatIcon > 0) {
                --clanChatIcon;
                var45 = true;

                for (int var11 = 0; var11 < clanChatIcon; ++var11) {
                    if (CS2Script.userCurrentWorldID != Unsorted.anIntArray882[var11] && Unsorted.anIntArray882[var11 - -1] == CS2Script.userCurrentWorldID || Unsorted.anIntArray882[var11] == 0 && Unsorted.anIntArray882[var11 - -1] != 0) {
                        var45 = false;
                        int var12 = Unsorted.anIntArray882[var11];
                        Unsorted.anIntArray882[var11] = Unsorted.anIntArray882[var11 - -1];
                        Unsorted.anIntArray882[1 + var11] = var12;
                        RSString var64 = Unsorted.aStringArray2566[var11];
                        Unsorted.aStringArray2566[var11] = Unsorted.aStringArray2566[var11 + 1];
                        Unsorted.aStringArray2566[var11 - -1] = var64;
                        RSString var57 = Class70.aStringArray1046[var11];
                        Class70.aStringArray1046[var11] = Class70.aStringArray1046[var11 + 1];
                        Class70.aStringArray1046[var11 - -1] = var57;
                        long var15 = Class50.aLongArray826[var11];
                        Class50.aLongArray826[var11] = Class50.aLongArray826[var11 - -1];
                        Class50.aLongArray826[var11 - -1] = var15;
                        int var17 = Class57.anIntArray904[var11];
                        Class57.anIntArray904[var11] = Class57.anIntArray904[var11 - -1];
                        Class57.anIntArray904[1 + var11] = var17;
                        boolean var18 = Unsorted.aBooleanArray73[var11];
                        Unsorted.aBooleanArray73[var11] = Unsorted.aBooleanArray73[var11 - -1];
                        Unsorted.aBooleanArray73[var11 - -1] = var18;
                    }
                }

                if (var45) {
                    break;
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_160)
        {
            if (0 == Unsorted.incomingPacketLength) {
                TextureOperation32.aString_3353 = TextCore.HasWalkHere;
            } else {
                TextureOperation32.aString_3353 = BufferedDataStream.incomingBuffer.readString();
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_128)
        {
            for (int nodeModelId = 0; nodeModelId < ItemDefinition.ram.length; ++nodeModelId) {
                if (ItemDefinition.ram[nodeModelId] != Class57.varpArray[nodeModelId]) {
                    ItemDefinition.ram[nodeModelId] = Class57.varpArray[nodeModelId];
                    Class46.method1087(98, nodeModelId);
                    Class44.anIntArray726[Unsorted.bitwiseAnd(Class36.anInt641++, 31)] = nodeModelId;
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_154)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var6 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int var30 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class146.updateInterfacePacketCounter(nodeModelId);
            Class3_Sub20.method390(true, var6, counter, var30, (byte) -124, modelId, var19);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_247)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            int nameAsLong = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var29 = BufferedDataStream.incomingBuffer.readMedium();
            int chatIcon = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int var33 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            boolean var49 = false;
            long var51 = (nameAsLong << 32) - -var29;
            int var59 = 0;

            label1603:
            while (true) {
                if (100 > var59) {
                    if (Class163_Sub2_Sub1.aLongArray4017[var59] != var51) {
                        ++var59;
                        continue;
                    }

                    var49 = true;
                    break;
                }

                if (chatIcon <= 1) {
                    for (var59 = 0; Class3_Sub28_Sub5.anInt3591 > var59; ++var59) {
                        if (var2 == Class114.ignores[var59]) {
                            var49 = true;
                            break label1603;
                        }
                    }
                }
                break;
            }

            if (!var49 && inTutorialIsland == 0) {
                Class163_Sub2_Sub1.aLongArray4017[MouseListeningClass.anInt1921] = var51;
                MouseListeningClass.anInt1921 = (1 + MouseListeningClass.anInt1921) % 100;
                RSString var64 = QuickChat.method733(var33).method555(BufferedDataStream.incomingBuffer);
                if (chatIcon == 2) {
                    MessageManager.sendGameMessage(var33, 18, var64, null, RSString.stringCombiner(new RSString[]{RSString.parse("<img=1>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}));
                } else if (1 == chatIcon) {
                    MessageManager.sendGameMessage(var33, 18, var64, null, RSString.stringCombiner(new RSString[]{RSString.parse("<img=0>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}));
                } else {
                    MessageManager.sendGameMessage(var33, 18, var64, null, Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString());
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_176)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV1();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int modelId = BufferedDataStream.incomingBuffer.readIntV1();
            Class146.updateInterfacePacketCounter(var19);
            Class3_Sub31 var23 = TextureOperation23.openInterfaces.get(nodeModelId);
            Class3_Sub31 var26 = TextureOperation23.openInterfaces.get(modelId);
            if (null != var26) {
                TextureOperation19.method254(null == var23 || var26.anInt2602 != var23.anInt2602, var26);
            }

            if (null != var23) {
                var23.unlink();
                TextureOperation23.openInterfaces.put(modelId, var23);
            }

            RSInterface var27 = Unsorted.getRSInterface(nodeModelId);
            if (var27 != null) {
                Class20.flagUpdate(var27);
            }

            var27 = Unsorted.getRSInterface(modelId);
            if (null != var27) {
                Class20.flagUpdate(var27);
                Unsorted.method2104(var27, true, 48);
            }

            if (ConfigInventoryDefinition.anInt3655 != -1) {
                Class3_Sub8.method124(28, 1, ConfigInventoryDefinition.anInt3655);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_27)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int var6 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int var30 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            Class146.updateInterfacePacketCounter(nodeModelId);
            WaterfallShader.aBooleanArray2169[var19] = true;
            TextureOperation14.anIntArray3383[var19] = modelId;
            Class166.anIntArray2073[var19] = counter;
            TextureOperation3.anIntArray3359[var19] = var6;
            Class163_Sub1_Sub1.anIntArray4009[var19] = var30;

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == RECEIVE_PM)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV1();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            Class146.updateInterfacePacketCounter(var19);
            CSConfigCachefile.method1385(modelId, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SYSTEM_UPDATE)
        {
            Class38_Sub1.anInt2617 = BufferedDataStream.incomingBuffer.readUnsignedShort() * 30;
            Unsorted.incomingOpcode = -1;
            Class140_Sub6.anInt2905 = PacketParser.anInt3213;
            return true;
        }

        if(opcode == INCOMING_PACKET_114)
        {
            TextureOperation3.method305(Class38.gameSignlink, BufferedDataStream.incomingBuffer, Unsorted.incomingPacketLength);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_VARC_SMALL)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedNegativeByte();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            Class146.updateInterfacePacketCounter(nodeModelId);
            TextureOperation19.method255(modelId, var19, (byte) -83 ^ -84);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == RUN_ENERGY_UPDATE)
        {
            BufferedDataStream.flagActiveInterfacesForUpdate();
            Unsorted.anInt136 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class140_Sub6.anInt2905 = PacketParser.anInt3213;
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_209)
        {
            if (-1 != ConfigInventoryDefinition.anInt3655) {
                Class3_Sub8.method124(48, 0, ConfigInventoryDefinition.anInt3655);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_191)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            CS2Methods.method532(nodeModelId);
            QuickChatDefinition.anIntArray3565[Unsorted.bitwiseAnd(31, Unsorted.anInt944++)] = Unsorted.bitwiseAnd(nodeModelId, 32767);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_102)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int var19 = BufferedDataStream.incomingBuffer.readUnsigned128Byte();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            NPC var39 = NPC.npcs[nodeModelId];
            if (null != var39) {
                Unsorted.method1772(var19, modelId, 39, var39);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_159)
        {
            BufferedDataStream.flagActiveInterfacesForUpdate();
            MouseListeningClass.anInt1925 = BufferedDataStream.incomingBuffer.readSignedShort();
            Class140_Sub6.anInt2905 = PacketParser.anInt3213;
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_71)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            RSString var58 = Font.method686(Objects.requireNonNull(Class32.method992(BufferedDataStream.incomingBuffer).properlyCapitalize()));
            BufferedDataStream.addChatMessage(Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString(), 6, var58, (byte) -83 ^ 82);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_42)
        {
            if (null != TextureOperation30.fullScreenFrame) {
                GameObject.graphicsSettings(false, Unsorted.anInt2577, -1, -1);
            }

            byte[] var22 = new byte[Unsorted.incomingPacketLength];
            BufferedDataStream.incomingBuffer.method811((byte) 30, 0, var22, Unsorted.incomingPacketLength);
            RSString playerName = TextureOperation33.bufferToString(var22, Unsorted.incomingPacketLength, 0);
            if (null == GameShell.frame && (3 == Signlink.anInt1214 || !Signlink.osName.startsWith("win") || Class106.paramUserUsingInternetExplorer)) {
                Class99.method1596(playerName, (byte) 127, true);
            } else {
                TextureOperation5.aString_3295 = playerName;
                Unsorted.aBoolean2154 = true;
                AudioThread.aClass64_351 = Class38.gameSignlink.method1452(new String(playerName.method1568(), StandardCharsets.ISO_8859_1), true);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_111)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int var19 = BufferedDataStream.incomingBuffer.readIntV2();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int var6 = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            Class146.updateInterfacePacketCounter(nodeModelId);
            PacketParser.method256(modelId, 7, var19, counter << 16 | var6);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_VARBIT_SMALL)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedByte128();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            VarpHelpers.setVarbit((byte) -122, nodeModelId, var19);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == OPEN_INTERFACE)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            int var19 = BufferedDataStream.incomingBuffer.readIntV2();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShort();
            Class146.updateInterfacePacketCounter(modelId);
            Class3_Sub31 var26 = TextureOperation23.openInterfaces.get(var19);
            if (null != var26) {
                TextureOperation19.method254(var26.anInt2602 != counter, var26);
            }

            Class21.method914(counter, var19, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_131)
        {
            for (int nodeModelId = 0; nodeModelId < Unsorted.players.length; ++nodeModelId) {
                if (Unsorted.players[nodeModelId] != null) {
                    Unsorted.players[nodeModelId].anInt2771 = -1;
                }
            }

            for (int nodeModelId = 0; nodeModelId < NPC.npcs.length; ++nodeModelId) {
                if (null != NPC.npcs[nodeModelId]) {
                    NPC.npcs[nodeModelId].anInt2771 = -1;
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_HINT_ICON)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class96 var48 = new Class96();
            int var19 = nodeModelId >> 6;
            var48.anInt1360 = nodeModelId & 63;
            var48.anInt1351 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            if (var48.anInt1351 >= 0 && Class166.aAbstractSpriteArray2072.length > var48.anInt1351) {
                if (var48.anInt1360 == 1 || 10 == var48.anInt1360) {
                    var48.anInt1359 = BufferedDataStream.incomingBuffer.readUnsignedShort();
                    BufferedDataStream.incomingBuffer.index += 3;
                } else if (var48.anInt1360 >= 2 && 6 >= var48.anInt1360) {
                    if (var48.anInt1360 == 2) {
                        var48.anInt1346 = 64;
                        var48.anInt1350 = 64;
                    }

                    if (var48.anInt1360 == 3) {
                        var48.anInt1346 = 0;
                        var48.anInt1350 = 64;
                    }

                    if (4 == var48.anInt1360) {
                        var48.anInt1346 = 128;
                        var48.anInt1350 = 64;
                    }

                    if (5 == var48.anInt1360) {
                        var48.anInt1346 = 64;
                        var48.anInt1350 = 0;
                    }

                    if (var48.anInt1360 == 6) {
                        var48.anInt1346 = 64;
                        var48.anInt1350 = 128;
                    }

                    var48.anInt1360 = 2;
                    var48.anInt1356 = BufferedDataStream.incomingBuffer.readUnsignedShort();
                    var48.anInt1347 = BufferedDataStream.incomingBuffer.readUnsignedShort();
                    var48.anInt1353 = BufferedDataStream.incomingBuffer.readUnsignedByte();
                }

                var48.anInt1355 = BufferedDataStream.incomingBuffer.readUnsignedShort();
                if (var48.anInt1355 == 65535) {
                    var48.anInt1355 = -1;
                }

                ClientErrorException.aClass96Array2114[var19] = var48;
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_IGNORE_LIST)
        {
            Class3_Sub28_Sub5.anInt3591 = Unsorted.incomingPacketLength / 8;

            for (int nodeModelId = 0; Class3_Sub28_Sub5.anInt3591 > nodeModelId; ++nodeModelId) {
                Class114.ignores[nodeModelId] = BufferedDataStream.incomingBuffer.readLong();
                TextureOperation7.aStringArray3341[nodeModelId] = Unsorted.method1052(Class114.ignores[nodeModelId]);
            }

            Class110.anInt1472 = PacketParser.anInt3213;
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == RENDER_NPC_PACKET)
        {
            NPCRendering.renderNPCs(8169);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == REPOSITION_COMPONENT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int var19 = BufferedDataStream.incomingBuffer.readIntLE();
            int modelId = BufferedDataStream.incomingBuffer.readSignedShort();
            int counter = BufferedDataStream.incomingBuffer.readSignedShort128();
            Class146.updateInterfacePacketCounter(nodeModelId);
            RSInterface.method2271(modelId, var19, counter);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_235)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsigned128Byte();
            int var19 = nodeModelId >> 2;
            int modelId = 3 & nodeModelId;
            int counter = Class75.anIntArray1107[var19];
            int var6 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var30 = BufferedDataStream.incomingBuffer.readInt();
            if (65535 == var6) {
                var6 = -1;
            }

            int clanChatIcon = 16383 & var30;
            int var33 = 16383 & var30 >> 14;
            var33 -= Class131.x1716;
            clanChatIcon -= Texture.y1152;
            int chatIcon = 3 & var30 >> 28;
            Class50.method1131(chatIcon, 110, modelId, var19, clanChatIcon, counter, var33, var6);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_0)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            int nameAsLong = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var29 = BufferedDataStream.incomingBuffer.readMedium();
            int chatIcon = BufferedDataStream.incomingBuffer.readUnsignedByte();
            boolean var42 = false;
            long var35 = var29 + (nameAsLong << 32);
            int var12 = 0;
            label1651:
            while (true) {
                if (var12 >= 100) {
                    if (chatIcon <= 1) {
                        if ((!Class3_Sub15.aBoolean2433 || Class121.aBoolean1641) && !TextureOperation31.aBoolean3166) {
                            for (var12 = 0; var12 < Class3_Sub28_Sub5.anInt3591; ++var12) {
                                if (Class114.ignores[var12] == var2) {
                                    var42 = true;
                                    break label1651;
                                }
                            }
                        } else {
                            var42 = true;
                        }
                    }
                    break;
                }

                if (Class163_Sub2_Sub1.aLongArray4017[var12] == var35) {
                    var42 = true;
                    break;
                }

                ++var12;
            }

            if (!var42 && inTutorialIsland == 0) {
                Class163_Sub2_Sub1.aLongArray4017[MouseListeningClass.anInt1921] = var35;
                MouseListeningClass.anInt1921 = (MouseListeningClass.anInt1921 - -1) % 100;
                RSString var52 = Font.method686(Objects.requireNonNull(Class32.method992(BufferedDataStream.incomingBuffer).properlyCapitalize()));
                if (chatIcon == 2 || chatIcon == 3) {
                    BufferedDataStream.addChatMessage(RSString.stringCombiner(new RSString[]{RSString.parse("<img=1>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}), 7, var52, -1);
                } else if (chatIcon == 1) {
                    BufferedDataStream.addChatMessage(RSString.stringCombiner(new RSString[]{RSString.parse("<img=" + (chatIcon - 1) + ">"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}), 7, var52, -1);
                } else {
                    BufferedDataStream.addChatMessage(RSString.stringCombiner(new RSString[]{RSString.parse("<img=" + (chatIcon - 1) + ">"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}), 7, var52, -1);
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_54)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            BufferedDataStream.incomingBuffer.readSignedByte();
            long nameAsLong = BufferedDataStream.incomingBuffer.readLong();
            int var29 = BufferedDataStream.incomingBuffer.readUnsignedShort();
            int var36 = BufferedDataStream.incomingBuffer.readMedium();
            long var44 = (var29 << 32) + var36;
            int clanChatIcon = BufferedDataStream.incomingBuffer.readUnsignedByte();
            boolean var13 = false;
            int var14 = 0;

            label1774:
            while (true) {
                if (var14 >= 100) {
                    if (1 >= clanChatIcon) {
                        if ((!Class3_Sub15.aBoolean2433 || Class121.aBoolean1641) && !TextureOperation31.aBoolean3166) {
                            for (var14 = 0; Class3_Sub28_Sub5.anInt3591 > var14; ++var14) {
                                if (Class114.ignores[var14] == var2) {
                                    var13 = true;
                                    break label1774;
                                }
                            }
                        } else {
                            var13 = true;
                        }
                    }
                    break;
                }

                if (Class163_Sub2_Sub1.aLongArray4017[var14] == var44) {
                    var13 = true;
                    break;
                }

                ++var14;
            }

            if (!var13 && 0 == inTutorialIsland) {
                Class163_Sub2_Sub1.aLongArray4017[MouseListeningClass.anInt1921] = var44;
                MouseListeningClass.anInt1921 = (MouseListeningClass.anInt1921 + 1) % 100;
                RSString var57 = Font.method686(Objects.requireNonNull(Class32.method992(BufferedDataStream.incomingBuffer).properlyCapitalize()));
                if (clanChatIcon == 2 || clanChatIcon == 3) {
                    TextureOperation1.method221(-1, var57, RSString.stringCombiner(new RSString[]{RSString.parse("<img=1>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}), Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), 9);
                } else if (clanChatIcon == 1) {
                    TextureOperation1.method221(-1, var57, RSString.stringCombiner(new RSString[]{RSString.parse("<img=0>"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}), Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), 9);
                } else {
                    if (clanChatIcon == 0)
                        TextureOperation1.method221(-1, var57, Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString(), Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), 9);
                    else
                        TextureOperation1.method221(-1, var57, RSString.stringCombiner(new RSString[]{RSString.parse("<img=" + (clanChatIcon - 1) + ">"), Objects.requireNonNull(Unsorted.method1052(var2)).longToRSString()}), Objects.requireNonNull(Unsorted.method1052(nameAsLong)).longToRSString(), 9);
                }
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == DYNAMIC_SCENE_GRAPH)
        {
            Class39.updateSceneGraph(true);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_172)
        {
            int soundEffectID;
            int soundEffectDelay;
            int soundEffectVolume;
            soundEffectDelay = BufferedDataStream.incomingBuffer.readUnsignedShort();
            soundEffectID = BufferedDataStream.incomingBuffer.readUnsignedByte();
            if (soundEffectDelay == 65535) {
                soundEffectDelay = -1;
            }

            soundEffectVolume = BufferedDataStream.incomingBuffer.readUnsignedShort();
            AudioHandler.soundEffectHandler(soundEffectID, soundEffectDelay, soundEffectVolume);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SEND_PLAYER_TO_COMPONENT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            int var19 = BufferedDataStream.incomingBuffer.readIntV1();
            Class146.updateInterfacePacketCounter(nodeModelId);
            int modelId = 0;
            if (Class102.player.class52 != null) {
                modelId = Class102.player.class52.method1163();
            }

            PacketParser.method256(-1, 3, var19, modelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_COMPONENT_TEXT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readIntV2();
            RSString playerName = BufferedDataStream.incomingBuffer.readString();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            Class146.updateInterfacePacketCounter(modelId);
            Unsorted.method566(playerName, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_VARBIT_LARGE)
        {
            int value = BufferedDataStream.incomingBuffer.readIntLE();
            int index = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            VarpHelpers.setVarbit((byte) -106, value, index);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_CONTAINER_SLOTBASED)
        {
            int componentHash = BufferedDataStream.incomingBuffer.readInt();
            int containerIdd = BufferedDataStream.incomingBuffer.readUnsignedShort();
            RSInterface iface;
            if (componentHash < -70000) {
                containerIdd += 32768;
            }

            if (componentHash < 0) {
                iface = null;
            } else {
                iface = Unsorted.getRSInterface(componentHash);
            }

            int slot;
            int itemAmount;
            int itemId;
            for (; Unsorted.incomingPacketLength > BufferedDataStream.incomingBuffer.index; PacketParser.addItemToContainer(itemAmount - 1, slot, itemId, containerIdd)) {
                slot = BufferedDataStream.incomingBuffer.getSmart();
                itemAmount = BufferedDataStream.incomingBuffer.readUnsignedShort();
                itemId = 0;
                if (itemAmount != 0) {
                    itemId = BufferedDataStream.incomingBuffer.readUnsignedByte();
                    if (itemId == 255) {
                        itemId = BufferedDataStream.incomingBuffer.readInt();
                    }
                }

                if (iface != null && slot >= 0 && slot < iface.itemIds.length) {
                    iface.itemIds[slot] = itemAmount;
                    iface.amounts[slot] = itemId;
                }
            }

            if (iface != null) {
                Class20.flagUpdate(iface);
            }

            BufferedDataStream.flagActiveInterfacesForUpdate();
            QuickChatDefinition.anIntArray3565[Unsorted.bitwiseAnd(Unsorted.anInt944++, 31)] = Unsorted.bitwiseAnd(32767, containerIdd);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_24)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            Class146.updateInterfacePacketCounter(nodeModelId);
            Class3_Sub28_Sub5.method560();

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == LOGOUT_PACKET)
        {
            XPGainDraw.reset();
            SlayerTracker.reset();
            Discord.updatePresence("At the login screen","","");
            Class167.method2269((byte) 46);
            Unsorted.incomingOpcode = -1;
            return false;
        }

        if(opcode == GRAND_EXCHANGE_UPDATE)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedByte();
            if (BufferedDataStream.incomingBuffer.readUnsignedByte() == 0) {
                TextureOperation29.aClass133Array3393[nodeModelId] = new Class133();
            } else {
                --BufferedDataStream.incomingBuffer.index;
                TextureOperation29.aClass133Array3393[nodeModelId] = new Class133(BufferedDataStream.incomingBuffer);
            }

            Unsorted.incomingOpcode = -1;
            Class121.anInt1642 = PacketParser.anInt3213;
            return true;
        }

        if(opcode == SEND_NPC_TO_COMPONENT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int var19 = BufferedDataStream.incomingBuffer.readIntLE();
            if (nodeModelId == 65535) {
                nodeModelId = -1;
            }

            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            Class146.updateInterfacePacketCounter(modelId);
            PacketParser.method256(-1, 2, var19, nodeModelId);

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_SCENE_GRAPH)
        {
            Class39.updateSceneGraph(false);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == SET_INTERFACE_SETTINGS)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            if (var19 == 65535) {
                var19 = -1;
            }

            int modelId = BufferedDataStream.incomingBuffer.readInt();
            int counter = BufferedDataStream.incomingBuffer.readUnsignedShort128();
            int var6 = BufferedDataStream.incomingBuffer.readIntV1();
            if (counter == 65535) {
                counter = -1;
            }

            Class146.updateInterfacePacketCounter(nodeModelId);

            Class3_Sub1 var38;

            for (int var30 = counter; var30 <= var19; ++var30) {
                long var36 = ((long) modelId << 32) - -((long) var30);
                Class3_Sub1 var47 = (Class3_Sub1) Class124.aHashTable_1659.get(var36);
                if (var47 == null) {
                    if (-1 == var30) {
                        var38 = new Class3_Sub1(var6, Objects.requireNonNull(Unsorted.getRSInterface(modelId)).aClass3_Sub1_257.anInt2202);
                    } else {
                        var38 = new Class3_Sub1(var6, -1);
                    }
                } else {
                    var38 = new Class3_Sub1(var6, var47.anInt2202);
                    var47.unlink();
                }

                Class124.aHashTable_1659.put(var36, var38);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_FRIENDLIST_STATE)
        {
            CS2Script.anInt1357 = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Class110.anInt1472 = PacketParser.anInt3213;
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_196)
        {
            long var2 = BufferedDataStream.incomingBuffer.readLong();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            byte var28 = BufferedDataStream.incomingBuffer.readSignedByte();
            boolean isIgnored = (Long.MIN_VALUE & var2) != 0;

            if (isIgnored) {
                if (Unsorted.clanSize == 0) {
                    Unsorted.incomingOpcode = -1;
                    return true;
                }

                var2 &= Long.MAX_VALUE;

                int var30;
                for (var30 = 0; var30 < Unsorted.clanSize && (var2 != PacketParser.aClass3_Sub19Array3694[var30].linkableKey || PacketParser.aClass3_Sub19Array3694[var30].anInt2478 != modelId); ++var30) {
                }

                if (var30 < Unsorted.clanSize) {
                    while (var30 < -1 + Unsorted.clanSize) {
                        PacketParser.aClass3_Sub19Array3694[var30] = PacketParser.aClass3_Sub19Array3694[1 + var30];
                        ++var30;
                    }

                    --Unsorted.clanSize;
                    PacketParser.aClass3_Sub19Array3694[Unsorted.clanSize] = null;
                }
            } else {
                RSString var41 = BufferedDataStream.incomingBuffer.readString();
                Class3_Sub19 var40 = new Class3_Sub19();
                var40.linkableKey = var2;
                var40.aString_2476 = Unsorted.method1052(var40.linkableKey);
                var40.aByte2472 = var28;
                var40.aString_2473 = var41;
                var40.anInt2478 = modelId;

                int clanChatIcon;
                int var33;
                for (var33 = -1 + Unsorted.clanSize; var33 >= 0; --var33) {
                    clanChatIcon = PacketParser.aClass3_Sub19Array3694[var33].aString_2476.method1559(var40.aString_2476);
                    if (clanChatIcon == 0) {
                        PacketParser.aClass3_Sub19Array3694[var33].anInt2478 = modelId;
                        PacketParser.aClass3_Sub19Array3694[var33].aByte2472 = var28;
                        PacketParser.aClass3_Sub19Array3694[var33].aString_2473 = var41;
                        if (PacketParser.aLong3202 == var2) {
                            Class91.aByte1308 = var28;
                        }

                        Class167.anInt2087 = PacketParser.anInt3213;
                        Unsorted.incomingOpcode = -1;
                        return true;
                    }

                    if (clanChatIcon < 0) {
                        break;
                    }
                }

                if (PacketParser.aClass3_Sub19Array3694.length <= Unsorted.clanSize) {
                    Unsorted.incomingOpcode = -1;
                    return true;
                }

                for (clanChatIcon = Unsorted.clanSize + -1; clanChatIcon > var33; --clanChatIcon) {
                    PacketParser.aClass3_Sub19Array3694[1 + clanChatIcon] = PacketParser.aClass3_Sub19Array3694[clanChatIcon];
                }

                if (Unsorted.clanSize == 0) {
                    PacketParser.aClass3_Sub19Array3694 = new Class3_Sub19[100];
                }

                PacketParser.aClass3_Sub19Array3694[1 + var33] = var40;
                if (PacketParser.aLong3202 == var2) {
                    Class91.aByte1308 = var28;
                }

                ++Unsorted.clanSize;
            }

            Unsorted.incomingOpcode = -1;
            Class167.anInt2087 = PacketParser.anInt3213;
            return true;
        }

        if(opcode == SEND_ITEM_TO_COMPONENT)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readInt();
            int var19 = BufferedDataStream.incomingBuffer.readIntV2();
            int modelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            if (65535 == modelId) {
                modelId = -1;
            }

            int counter = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            Class146.updateInterfacePacketCounter(counter);
            RSInterface var34 = Unsorted.getRSInterface(var19);
            ItemDefinition var43;
            if (Objects.requireNonNull(var34).usingScripts) {
                Class140_Sub6.method2026(var19, nodeModelId, modelId);
                var43 = ItemDefinition.getItemDefinition(modelId);
                Unsorted.method2143((byte) -128, var43.anInt810, var19, var43.anInt799, var43.anInt786);
                Class84.method1420(var19, var43.anInt768, var43.anInt754, var43.anInt792);
            } else {
                if (-1 == modelId) {
                    var34.modelType = 0;
                    Unsorted.incomingOpcode = -1;
                    return true;
                }

                var43 = ItemDefinition.getItemDefinition(modelId);
                var34.anInt182 = var43.anInt786;
                var34.anInt164 = 100 * var43.anInt810 / nodeModelId;
                var34.modelType = 4;
                var34.itemId = modelId;
                var34.anInt308 = var43.anInt799;
                Class20.flagUpdate(var34);
            }

            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_CONTAINER_NONSLOT)
        {
            int componentHash = BufferedDataStream.incomingBuffer.readInt();
            int containerId = BufferedDataStream.incomingBuffer.readUnsignedShort();
            RSInterface iface;
            int counter;
            int slot;
            int amount;

            if (componentHash < -70000) {
                containerId += 32768;
            }

            if (0 <= componentHash) {
                iface = Unsorted.getRSInterface(componentHash);
            } else {
                iface = null;
            }

            if (iface != null) {
                for (counter = 0; iface.itemIds.length > counter; ++counter) {
                    iface.itemIds[counter] = 0;
                    iface.amounts[counter] = 0;
                }
            }

            CS2Methods.resetContainer(containerId);
            counter = BufferedDataStream.incomingBuffer.readUnsignedShort();

            for (slot = 0; counter > slot; ++slot) {
                amount = BufferedDataStream.incomingBuffer.readUnsigned128Byte();
                if (255 == amount) {
                    amount = BufferedDataStream.incomingBuffer.readInt();
                }

                int itemId = BufferedDataStream.incomingBuffer.readUnsignedShort();
                if (null != iface && iface.itemIds.length > slot) {
                    iface.itemIds[slot] = itemId;
                    iface.amounts[slot] = amount;
                }

                PacketParser.addItemToContainer(-1 + itemId, slot, amount, containerId);
            }

            if (iface != null) {
                Class20.flagUpdate(iface);
            }

            BufferedDataStream.flagActiveInterfacesForUpdate();
            QuickChatDefinition.anIntArray3565[Unsorted.bitwiseAnd(Unsorted.anInt944++, 31)] = Unsorted.bitwiseAnd(32767, containerId);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == INCOMING_PACKET_142)
        {
            LinkableRSString.method734(BufferedDataStream.incomingBuffer.readString());
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == UPDATE_CURRENT_LOCATION)
        {
            Class39.currentChunkX = BufferedDataStream.incomingBuffer.readUnsignedNegativeByte();
            Class39.currentChunkY = BufferedDataStream.incomingBuffer.readUnsignedByte();
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == PLAY_SONG)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.readUnsignedShortLE128();
            if (nodeModelId == 65535) {
                nodeModelId = -1;
            }

            AudioHandler.musicHandler(nodeModelId);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        if(opcode == PLAY_JINGLE)
        {
            int nodeModelId = BufferedDataStream.incomingBuffer.getTriByte2();
            int var19 = BufferedDataStream.incomingBuffer.readUnsignedShortLE();
            if (var19 == 65535) {
                var19 = -1;
            }

            AudioHandler.musicEffectHandler(var19);
            Unsorted.incomingOpcode = -1;
            return true;
        }

        Class49.reportError("T1 - " + Unsorted.incomingOpcode + "," + Class7.anInt2166 + "," + Class24.anInt469 + " - " + Unsorted.incomingPacketLength, null);
        Class167.method2269((byte) 46);
        return true;
    }
}
